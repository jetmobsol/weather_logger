package com.jms.weather.app.di.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import com.jms.weather.app.WeatherApp
import com.jms.weather.app.di.module.*
import com.jms.weather.feature.current.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        WeatherDatabaseModule::class,
        ViewModelFactoryModule::class,
        NetModule::class,

        WeatherActivityBuilder::class,
        WeatherRepositoryModule::class,
        WeatherViewModelModule::class,
        WeatherApiModule::class,
        WeatherDaoModule::class
    ]
)
interface AppComponent : AndroidInjector<WeatherApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun create(application: Application): Builder

        fun build(): AppComponent
    }

    override fun inject(app: WeatherApp)
}