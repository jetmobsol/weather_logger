package com.jms.weather.app.di.module

import android.app.Application
import com.jms.weather.feature.current.data.local.WeatherDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class WeatherDatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(application: Application) = WeatherDatabase.getInstance(application)

}