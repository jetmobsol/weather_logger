package com.jms.weather.feature.current.data.local

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

/**
 * Data class for Database entity and Serialization.
 */
@Entity(tableName = DbLocation.TABLE_NAME)
data class DbLocation(
    @PrimaryKey(autoGenerate = true)
    var id: Long,
    var remoteId: Long = 0L,
    var locationName: String? = null,
    var lat: Double? = null,
    var lon: Double? = null,
    var country: String? = null
) {
    companion object {
        const val TABLE_NAME = "location"
    }

    @Ignore
    val weatherList = mutableListOf<DbCurrentWeather>()
    @Ignore
    var recordCount = 0
}

@Entity(tableName = DbCurrentWeather.TABLE_NAME)
data class DbCurrentWeather(
    @PrimaryKey(autoGenerate = true)
    var id: Long,
    var locationId: Long=0L,
    var requestTime: Long = System.currentTimeMillis(),
    var serverTime: Long = 0L,
    var temperature: Double? = null,
    var icon: String? = null
) {
    companion object {
        const val TABLE_NAME = "current_weather"
    }
}