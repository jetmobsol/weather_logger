package com.jms.weather.feature.current.presentation.locations.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jms.weather.databinding.LocationsItemBinding
import com.jms.weather.feature.current.domain.model.CurrentWeatherDomainModel

/**
 * Adapter class [RecyclerView.Adapter] for [RecyclerView] which binds [CurrentWeatherDomainModel] along with [LocationViewHolder]
 * @param onItemClickListener Listener which will receive callback when item is clicked.
 */
class LocationListAdapter(private val onItemClickListener: OnItemClickListener) :
    ListAdapter<CurrentWeatherDomainModel, LocationViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = LocationViewHolder(
        LocationsItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: LocationViewHolder, position: Int) =
        holder.bind(getItem(position), onItemClickListener)

    interface OnItemClickListener {
        fun onItemClicked(currentWeather: CurrentWeatherDomainModel, imageView: ImageView)
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<CurrentWeatherDomainModel>() {
            override fun areItemsTheSame(oldItem: CurrentWeatherDomainModel, newItem: CurrentWeatherDomainModel): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: CurrentWeatherDomainModel, newItem: CurrentWeatherDomainModel): Boolean =
                oldItem == newItem

        }
    }
}
