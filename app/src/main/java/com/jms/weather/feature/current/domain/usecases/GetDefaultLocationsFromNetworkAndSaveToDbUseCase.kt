package com.jms.weather.feature.current.domain.usecases

import com.jms.weather.feature.current.domain.repository.WeatherRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@Singleton
class GetDefaultLocationsFromNetworkAndSaveToDbUseCase @Inject constructor(
    private val weatherRepository: WeatherRepository
) {

    fun getDefaultCountriesWeather() = weatherRepository.getDefaultCountriesWeather()

}
