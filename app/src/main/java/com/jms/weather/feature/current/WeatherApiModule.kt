package com.jms.weather.feature.current

import android.os.Environment
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.jms.weather.BuildConfig
import com.jms.weather.feature.current.data.remote.api.DefaultRequestInterceptor
import com.jms.weather.feature.current.data.remote.api.OpenWeatherMapService
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class WeatherApiModule {

    @Singleton
    @Provides
    @Named("weather_cached")
    fun provideOkHttpClient(): OkHttpClient {
        val cache = Cache(Environment.getDownloadCacheDirectory(), 10 * 1024 * 1024)
        return OkHttpClient.Builder()
            .addNetworkInterceptor(StethoInterceptor())
            .addInterceptor(DefaultRequestInterceptor())
            .readTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES)
            .cache(cache)
            .build()
    }


    @Singleton
    @Provides
    fun provideRetrofitService(@Named("weather_cached") okHttpClient: OkHttpClient): OpenWeatherMapService = Retrofit.Builder()
        .baseUrl(BuildConfig.OPEN_WEATHER_MAP_URL)
        .addConverterFactory(
            MoshiConverterFactory.create(
                Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
            )
        )
        .client(okHttpClient)
        .build()
        .create(OpenWeatherMapService::class.java)
}
