package com.jms.weather.feature.current.data.remote.response

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize


data class CurrentWeatherGroupResponse(
    val cnt: Int? = null,
    val list:List<CurrentWeatherResponse>? = null

)

data class CurrentWeatherResponse(

    @Json(name = "coord")
    val coord: Coord? = null,

    @Json(name = "sys")
    val sys: Sys? = null,

    @Json(name = "weather")
    val weather: List<WeatherItem?>? = null,

    @Json(name = "main")
    val main: Main? = null,

    @Json(name = "wind")
    val wind: Wind? = null,

    @Json(name = "clouds")
    val clouds: Clouds? = null,

    @Json(name = "dt")
    val dt: Int? = null,

    @Json(name = "id")
    val id: Int? = null,

    @Json(name = "name")
    val name: String? = null

)



@Parcelize
@JsonClass(generateAdapter = true)
data class Main(

    @Json(name = "temp")
    val temp: Double?,

    @Json(name = "feels_like")
    val feelsLike: Double?,

    @Json(name = "temp_min")
    var tempMin: Double?,

    @Json(name = "temp_max")
    var tempMax: Double?,

    @Json(name = "pressure")
    val pressure: Double?,

    @Json(name = "humidity")
    val humidity: Int?

    /*@Json(name = "grnd_level")
    val grndLevel: Double?,

    @Json(name = "temp_kf")
    val tempKf: Double?,

    @Json(name = "sea_level")
    val seaLevel: Double?*/

) : Parcelable {

    fun getTempString(): String {
        return temp.toString().substringBefore(".") + "°"
    }

    fun getHumidityString(): String {
        return humidity.toString() + "°"
    }

    fun getTempMinString(): String {
        return tempMin.toString().substringBefore(".") + "°"
    }

    fun getTempMaxString(): String {
        return tempMax.toString().substringBefore(".") + "°"
    }
}

@Parcelize
@JsonClass(generateAdapter = true)
data class Clouds(
    @Json(name = "all")
    val all: Int?
) : Parcelable

@Parcelize
@JsonClass(generateAdapter = true)
data class Sys(

    @Json(name = "country")
    val country: String?,

    @Json(name = "timezone")
    val timezone: Long?,

    @Json(name = "sunrise")
    val sunrise: Long?,

    @Json(name = "sunset")
    val sunset: Long?

) : Parcelable

@Parcelize
@JsonClass(generateAdapter = true)
data class Coord(

    @Json(name = "lon")
    val lon: Double?,

    @Json(name = "lat")
    val lat: Double?
) : Parcelable


@Parcelize
@JsonClass(generateAdapter = true)
data class WeatherItem(

    @Json(name = "icon")
    val icon: String?,

    @Json(name = "description")
    val description: String?,

    @Json(name = "main")
    val main: String?,

    @Json(name = "id")
    val id: Int?
) : Parcelable {

    fun getDescriptionText(): String? {
        return description?.capitalize()
    }
}

@Parcelize
@JsonClass(generateAdapter = true)
data class Wind(
    @Json(name = "deg")
    val deg: Double?,

    @Json(name = "speed")
    val speed: Double?
): Parcelable



