package com.jms.weather.feature.current

import androidx.lifecycle.ViewModel
import com.jms.weather.app.di.module.ViewModelKey
import com.jms.weather.feature.current.presentation.locations.LocationsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
@Module
abstract class WeatherViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(LocationsViewModel::class)
    abstract fun bindLocationsViewModel(viewModel: LocationsViewModel): ViewModel

}