package com.jms.weather.feature.current.presentation.locations.adapter

import android.graphics.Color
import android.os.Build
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.jms.weather.R
import com.jms.weather.databinding.LocationsItemBinding

import com.jms.weather.feature.current.domain.model.CurrentWeatherDomainModel
import com.perfomer.blitz.setTimeAgo

/**
 * [RecyclerView.ViewHolder] implementation to inflate View for RecyclerView.
 * See [LocationListAdapter]]
 */
class LocationViewHolder(private val binding: LocationsItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(
        currentWeather: CurrentWeatherDomainModel,
        onItemClickListener: LocationListAdapter.OnItemClickListener? = null
    ) {
        binding.locationName.text = currentWeather.name
        binding.requestDate.text = currentWeather.date
        binding.temp.text = currentWeather.temperature
        binding.recordCount.text = binding.root.context.getString(R.string.record_count, currentWeather.recordCount)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.timeAgo.setTimeAgo(currentWeather.dateAsMilis, showSeconds = true)
        } else {
            binding.timeAgo.visibility = View.GONE
        }

        if (currentWeather.icon != null) {
            val newPath =
                currentWeather.icon.replace(currentWeather.icon, "a${currentWeather.icon}") + "_svg"
            val imageid = binding.imageView.context.resources.getIdentifier(
                newPath,
                "drawable",
                binding.imageView.context.packageName
            )
            val imageDrawable = ContextCompat.getDrawable(binding.imageView.context, imageid)
            binding.imageView.setImageDrawable(imageDrawable)
            binding.imageView.setBackgroundColor(getColor(newPath))
        }

        onItemClickListener?.let { listener ->
            binding.root.setOnClickListener {
                listener.onItemClicked(currentWeather, binding.imageView)
            }
        }
    }


    fun getColor(svg: String): Int {
        return when (svg) {

            "a01d_svg" -> Color.parseColor("#76FF03")
            "a01n_svg" -> Color.parseColor("#FF0090")
            "a02d_svg" -> Color.parseColor("#FFAE00")
            "a02n_svg" -> Color.parseColor("#0090FF")
            "a03d_svg" -> Color.parseColor("#DC0000")
            "a03n_svg" -> Color.parseColor("#0051FF")
            "a04d_svg" -> Color.parseColor("#3D28E0")
            "a04n_svg" -> Color.parseColor("#bb86fc")
            "a09d_svg" -> Color.parseColor("#6200ee")
            "a09n_svg" -> Color.parseColor("#b01d0")
            "a10d_svg" -> Color.parseColor("#4b01d0")
            "a10n_svg" -> Color.parseColor("#3700b3")
            "a11d_svg" -> Color.parseColor("#03dac6")
            "a11n_svg" -> Color.parseColor("#018786")
            "a13d_svg" -> Color.parseColor("#cf6679")
            "a13n_svg" -> Color.parseColor("#b00020")
            "a50d_svg" -> Color.parseColor("#AEEA00")
            "a50n_svg" -> Color.parseColor("#F57F17")
            "a1232n_svg" -> Color.parseColor("#607D8B")

            else -> Color.parseColor("#28E0AE")
        }
    }
}
