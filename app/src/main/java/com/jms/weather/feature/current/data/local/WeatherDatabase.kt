package com.jms.weather.feature.current.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.jms.weather.feature.current.data.local.dao.CurrentWeatherDao
import com.jms.weather.feature.current.data.local.dao.LocationDao




@Database(
    entities = [DbLocation::class, DbCurrentWeather::class],
    version = DatabaseMigrations.DB_VERSION
)

abstract class WeatherDatabase : RoomDatabase() {

    /**
     * @return [CurrentWeatherDao] Current Weather Data Access Object.
     * */

    abstract fun getCurrentWeatherDao(): CurrentWeatherDao

    abstract fun getLocationDao(): LocationDao

    companion object {
        const val DB_NAME = "weather_database"

        @Volatile
        private var INSTANCE: WeatherDatabase? = null

        fun getInstance(context: Context): WeatherDatabase {
            val tempInstance =
                INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        WeatherDatabase::class.java,
                    DB_NAME
                    )
                    .addMigrations(*DatabaseMigrations.MIGRATIONS)
                    .build()

                INSTANCE = instance
                return instance
            }
        }

    }
}
