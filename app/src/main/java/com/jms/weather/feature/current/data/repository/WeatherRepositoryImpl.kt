package com.jms.weather.feature.current.data.repository

import com.jms.weather.app.data.NetworkBoundRepository
import com.jms.weather.app.utils.State
import com.jms.weather.feature.current.data.local.DbLocation
import com.jms.weather.feature.current.data.local.dao.CurrentWeatherDao
import com.jms.weather.feature.current.data.local.dao.LocationDao
import com.jms.weather.feature.current.data.remote.api.DEFAULT_TOWN_LIST
import com.jms.weather.feature.current.data.remote.api.OpenWeatherMapService
import com.jms.weather.feature.current.data.remote.response.CurrentWeatherGroupResponse
import com.jms.weather.feature.current.data.toDbLocationList
import com.jms.weather.feature.current.data.toLocationDomainModelList
import com.jms.weather.feature.current.domain.model.CurrentWeatherDomainModel
import com.jms.weather.feature.current.domain.repository.WeatherRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import javax.inject.Inject

/**
 * Singleton repository for fetching data from remote and storing it in database
 * for offline capability. This is Single source of data.
 */
@ExperimentalCoroutinesApi
class WeatherRepositoryImpl @Inject constructor(
    private val locationDao: LocationDao,
    private val currentWeatherDao: CurrentWeatherDao,
    private val openWeatherMapService: OpenWeatherMapService
) : WeatherRepository {

    override fun getDefaultCountriesWeather(): Flow<State<List<CurrentWeatherDomainModel>>> {
        return getCurrentWeatherForGroup(DEFAULT_TOWN_LIST)
    }

    override fun getCurrentWeatherForGroup(idList: List<Long>): Flow<State<List<CurrentWeatherDomainModel>>> {
        return object :
            NetworkBoundRepository<List<DbLocation>, CurrentWeatherGroupResponse>() {

            override fun fetchFromLocal(): Flow<List<DbLocation>> =
                getAllLocationsWithLatestWeather()

            override suspend fun fetchFromRemote() =
                openWeatherMapService.getCurrentWeatherForGroup(idList.joinToString(separator = ","))

            override suspend fun saveRemoteData(response: CurrentWeatherGroupResponse) {
                response.toDbLocationList()
                    .let { if (it.isNotEmpty()) insertLocations(it) }
            }
        }.asFlow()
            .domainMap()
            .flowOn(Dispatchers.IO)
    }

    override fun getAllLocationsFromDb(): Flow<State<List<CurrentWeatherDomainModel>>> {
        return flow {
            emit(State.success(getAllLocationsWithLatestWeather().first()))
        }.domainMap()
            .flowOn(Dispatchers.IO)
    }

    override fun deleteAllData() =
        flow {
            try {
                locationDao.deleteAllLocations()
                currentWeatherDao.deleteAllWeathers()
                emit(true)
            } catch (e: Exception) {
                e.printStackTrace()
                emit(false)
            }
        }.flowOn(Dispatchers.IO)

    //Internals
    private fun insertLocations(newLocations: List<DbLocation>) {
        newLocations.forEach { newLocation ->
            val foundLoc = locationDao.getItemByRemoteId(newLocation.remoteId)
            val locationId = foundLoc?.id ?: locationDao.insertLocation(newLocation)
            newLocation.weatherList.forEach { newWeather ->
                currentWeatherDao.insertCurrentWeather(newWeather.copy(locationId = locationId))
            }
        }
    }

    private fun getAllLocationsWithLatestWeather(): Flow<List<DbLocation>> {
        return locationDao
            .getAllLocations().map {
                it.forEach { location ->
                    currentWeatherDao.getLatestWeatherByLocationId(location.id)?.also { weather ->
                        location.weatherList.add(weather)
                        location.recordCount = currentWeatherDao.countByLocationId(location.id)
                    }
                }
                it
            }
    }


    private fun Flow<State<List<DbLocation>>>.domainMap(): Flow<State<List<CurrentWeatherDomainModel>>> {
        return this.map { weather ->
            when (weather) {
                is State.Success -> {
                    State.success(weather.data.toLocationDomainModelList())
                }
                is State.Error -> {
                    State.error<List<CurrentWeatherDomainModel>>(weather.message)
                }
                is State.Loading -> {
                    State.loading()
                }
            }
        }
    }


}
