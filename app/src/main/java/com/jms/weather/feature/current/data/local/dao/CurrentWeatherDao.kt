package com.jms.weather.feature.current.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jms.weather.feature.current.data.local.DbCurrentWeather
import kotlinx.coroutines.flow.Flow

/**
 * Data Access Object (DAO) for [com.jms.weather.feature.current.data.local.WeatherDatabase]
 */
@Dao
interface CurrentWeatherDao {

    /**
     * Inserts [DbCurrentWeather] into the [DbCurrentWeather.TABLE_NAME] table.
     * Duplicate values are replaced in the table.
     * @param weathers DbCurrentWeather
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCurrentWeathers(weathers: List<DbCurrentWeather>)


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCurrentWeather(weather: DbCurrentWeather)

    /**
     * Deletes all the items from the [DbCurrentWeather.TABLE_NAME] table.
     */
    @Query("DELETE FROM ${DbCurrentWeather.TABLE_NAME}")
    fun deleteAllWeathers()

    /**
     * Fetches all the current weathers from the [DbCurrentWeather.TABLE_NAME] table.
     * @return [Flow]
     */
    @Query("SELECT * FROM ${DbCurrentWeather.TABLE_NAME} order by requestTime desc")
    fun getAllCurrentWeathers(): Flow<List<DbCurrentWeather>>

    @Query("SELECT * FROM ${DbCurrentWeather.TABLE_NAME} where locationId = :locationId order by id desc LIMIT 1")
    fun getLatestWeatherByLocationId(locationId:Long): DbCurrentWeather?

    @Query("SELECT count(1) FROM ${DbCurrentWeather.TABLE_NAME} where locationId = :locationId ")
    fun countByLocationId(locationId:Long): Int

}