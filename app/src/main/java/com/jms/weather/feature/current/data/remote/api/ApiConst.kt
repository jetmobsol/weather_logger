package com.jms.weather.feature.current.data.remote.api

const val RIGA_TOWN_ID = 456173L
const val JELGAVA_TOWN_ID = 459279L
const val VENTSPILS_TOWN_ID = 454310L
const val JURMALA_TOWN_ID = 459201L
const val SAULKRASTI_TOWN_ID = 455812L
const val NEW_YOURK_TOWN_ID = 5128581L


const val UNITS_METRIC = "metric"

val DEFAULT_TOWN_LIST = listOf(
    RIGA_TOWN_ID,
    JELGAVA_TOWN_ID,
    VENTSPILS_TOWN_ID,
    JURMALA_TOWN_ID,
    SAULKRASTI_TOWN_ID,
    NEW_YOURK_TOWN_ID)