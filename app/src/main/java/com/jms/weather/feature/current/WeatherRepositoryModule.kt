package com.jms.weather.feature.current

import com.jms.weather.feature.current.data.local.dao.CurrentWeatherDao
import com.jms.weather.feature.current.data.local.dao.LocationDao
import com.jms.weather.feature.current.data.remote.api.OpenWeatherMapService
import com.jms.weather.feature.current.data.repository.WeatherRepositoryImpl
import com.jms.weather.feature.current.domain.repository.WeatherRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class WeatherRepositoryModule {

    @Singleton
    @Provides
    fun provideWeatherRepository(locationDao: LocationDao,
                                  currentWeatherDao: CurrentWeatherDao,
                                  openWeatherMapService: OpenWeatherMapService): WeatherRepository {
        return  WeatherRepositoryImpl (locationDao, currentWeatherDao, openWeatherMapService)
    }

}