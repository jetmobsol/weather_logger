package com.jms.weather.feature.current

import com.jms.weather.feature.current.presentation.locations.LocationsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector
import kotlinx.coroutines.ExperimentalCoroutinesApi

@Module
abstract class WeatherActivityBuilder {

    @ExperimentalCoroutinesApi
    @ContributesAndroidInjector
    abstract fun bindLocationsActivity(): LocationsActivity

}
