package com.jms.weather.feature.current.domain.repository

import com.jms.weather.app.utils.State
import com.jms.weather.feature.current.domain.model.CurrentWeatherDomainModel
import kotlinx.coroutines.flow.Flow

interface WeatherRepository {

    fun getCurrentWeatherForGroup(idList: List<Long>): Flow<State<List<CurrentWeatherDomainModel>>>

    fun getDefaultCountriesWeather(): Flow<State<List<CurrentWeatherDomainModel>>>

    fun getAllLocationsFromDb(): Flow<State<List<CurrentWeatherDomainModel>>>

    fun deleteAllData():Flow<Boolean>

}
