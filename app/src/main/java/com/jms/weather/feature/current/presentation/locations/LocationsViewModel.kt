package com.jms.weather.feature.current.presentation.locations

import androidx.lifecycle.*

import com.jms.weather.app.utils.State
import com.jms.weather.feature.current.domain.model.CurrentWeatherDomainModel
import com.jms.weather.feature.current.domain.usecases.DeleteAllDataUseCase
import com.jms.weather.feature.current.domain.usecases.GetAllLocationsFromDbUseCase
import com.jms.weather.feature.current.domain.usecases.GetDefaultLocationsFromNetworkAndSaveToDbUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * ViewModel for [LocationsActivity]
 */
@ExperimentalCoroutinesApi
class LocationsViewModel @Inject constructor(
    private val deleteAllDataUseCase: DeleteAllDataUseCase,
    private val getDefaultLocationsFromNetworkAndSaveToDbUseCase: GetDefaultLocationsFromNetworkAndSaveToDbUseCase,
    private val getAllLocationsFromDbUseCase: GetAllLocationsFromDbUseCase
    ) :
    ViewModel() {
    private val _locationsLiveData = MutableLiveData<State<List<CurrentWeatherDomainModel>>>()

    val locationsLiveData: LiveData<State<List<CurrentWeatherDomainModel>>>
        get() = _locationsLiveData


    fun deleteAllData(){
        viewModelScope.launch {
            deleteAllDataUseCase
                .deleteAllData()
                .collect{
                    if(it) _locationsLiveData.value = State.success(listOf())
                }
        }
    }

    fun getAllLocations() {
        viewModelScope.launch {
            getDefaultLocationsFromNetworkAndSaveToDbUseCase
                .getDefaultCountriesWeather()
                .debounce(1000)
                .collect {
                _locationsLiveData.value = it
            }
        }
    }

    fun getAllLocationsFromDb() {
        viewModelScope.launch {
            getAllLocationsFromDbUseCase.getAllLocationsFromDb().collect {
                _locationsLiveData.value = it
            }
        }
    }

}
