package com.jms.weather.feature.current.presentation.locations

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.Observer
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.jms.weather.R
import com.jms.weather.app.presentation.BaseActivity
import com.jms.weather.app.utils.*
import com.jms.weather.databinding.LocationsActivityBinding
import com.jms.weather.feature.current.domain.model.CurrentWeatherDomainModel
import com.jms.weather.feature.current.presentation.locations.adapter.LocationListAdapter
import kotlinx.android.synthetic.main.locations_activity.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import ru.ldralighieri.corbind.view.clicks

@ExperimentalCoroutinesApi
class LocationsActivity : BaseActivity<LocationsViewModel, LocationsActivityBinding>(),
    LocationListAdapter.OnItemClickListener {

    private val mAdapter: LocationListAdapter by lazy { LocationListAdapter(onItemClickListener = this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_MyApp)  // Set AppTheme before setting content view.

        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)

        mViewBinding.topAppBar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.action_delete -> {
                    MaterialAlertDialogBuilder(this)
                        .setTitle(R.string.dlg_delete_all_title)
                        .setMessage(R.string.dlg_delete_all_msg)
                        .setPositiveButton(android.R.string.yes) { dialog, _ ->
                            mViewModel.deleteAllData()
                            dialog.dismiss()
                        }
                        .setNegativeButton(android.R.string.no) { dialog, _ ->
                            dialog.dismiss()
                        }
                        .show()
                    true
                }

                R.id.action_theme -> {
                    // Handle favorite icon press
                    // Get new mode.
                    val mode =
                        if ((resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) ==
                            Configuration.UI_MODE_NIGHT_NO
                        ) {
                            AppCompatDelegate.MODE_NIGHT_YES
                        } else {
                            AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY
                        }

                    // Change UI Mode
                    AppCompatDelegate.setDefaultNightMode(mode)
                    true
                }

                R.id.action_save -> {
                    getAllLocations()
                    true
                }

                else -> false
            }
        }
        // Initialize RecyclerView
        mViewBinding.postsRecyclerView.apply {
            layoutManager = LinearLayoutManager(this@LocationsActivity)
            adapter = mAdapter
        }

        initState()

        handleNetworkChanges()
    }

    private fun initState() {
        mViewModel.locationsLiveData.observe(this, Observer { state ->
            when (state) {
                is State.Loading -> showLoading(true)
                is State.Success -> {
                    if (state.data.isNotEmpty()) {
                        mViewBinding.btnLoadData.visibility = View.GONE
                        mAdapter.submitList(state.data.toMutableList())
                        showLoading(false)
                    } else {
                        mAdapter.submitList(state.data.toMutableList())
                        mViewBinding.btnLoadData.visibility = View.VISIBLE
                    }
                }
                is State.Error -> {
                    showToast(state.message)
                    showLoading(false)
                }
            }
        })

        mViewBinding.swipeRefreshLayout.setOnRefreshListener {
            getAllLocations()
        }

        mViewBinding.btnLoadData.setOnClickListener {
            getAllLocations()
        }

        /*mViewBinding.btnLoadData.clicks()
            .debounce(1000)
            .onEach { getAllLocations() }
            .launchIn(mViewModel.viewModelScope)*/
    }

    private fun getAllLocations() {
        mViewModel.getAllLocations()
    }

    private fun showLoading(isLoading: Boolean) {
        if (isLoading) {
            mViewBinding.btnLoadData.visibility = View.GONE
            mViewBinding.pbLoading.visibility = View.VISIBLE
        } else {
            mViewBinding.pbLoading.visibility = View.GONE
        }
        mViewBinding.swipeRefreshLayout.isRefreshing = isLoading
    }

    /**
     * Observe network changes i.e. Internet Connectivity
     */
    private fun handleNetworkChanges() {
        NetworkUtils.getNetworkLiveData(applicationContext)
            .observe(this, Observer { isConnected ->
                if (!isConnected) {
                    mViewBinding.textViewNetworkStatus.text =
                        getString(R.string.text_no_connectivity)
                    mViewBinding.networkStatusLayout.apply {
                        show()
                        setBackgroundColor(getColorRes(R.color.colorStatusNotConnected))
                    }
                } else {
                    mViewModel.getAllLocationsFromDb()
                    mViewBinding.textViewNetworkStatus.text =
                        getString(R.string.text_connectivity)
                    mViewBinding.networkStatusLayout.apply {
                        setBackgroundColor(getColorRes(R.color.colorStatusConnected))

                        animate()
                            .alpha(1f)
                            .setStartDelay(ANIMATION_DURATION)
                            .setDuration(ANIMATION_DURATION)
                            .setListener(object : AnimatorListenerAdapter() {
                                override fun onAnimationEnd(animation: Animator) {
                                    hide()
                                }
                            })
                    }
                }
            })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_theme -> {
                // Get new mode.
                val mode =
                    if ((resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) ==
                        Configuration.UI_MODE_NIGHT_NO
                    ) {
                        AppCompatDelegate.MODE_NIGHT_YES
                    } else {
                        AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY
                    }

                // Change UI Mode
                AppCompatDelegate.setDefaultNightMode(mode)
                true
            }
            else -> true
        }
    }


    override fun onBackPressed() {
        MaterialAlertDialogBuilder(this)
            .setTitle(R.string.dlg_exit_title)
            .setMessage(R.string.dlg_exit_msg)
            .setPositiveButton(android.R.string.yes) { dialog, _ ->
                dialog.dismiss()
                super.onBackPressed()
            }
            .setNegativeButton(android.R.string.no) { dialog, _ ->
                dialog.dismiss()
            }
            .show()

    }

    override fun getViewBinding(): LocationsActivityBinding =
        LocationsActivityBinding.inflate(layoutInflater)

    override fun getViewModel() = viewModelOf<LocationsViewModel>(mViewModelProvider)

    companion object {
        const val ANIMATION_DURATION = 1000.toLong()
    }

    override fun onItemClicked(
        weather: CurrentWeatherDomainModel,
        imageView: ImageView
    ) {
    }
}
