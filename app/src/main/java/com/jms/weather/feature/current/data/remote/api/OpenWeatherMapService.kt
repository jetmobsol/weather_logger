package com.jms.weather.feature.current.data.remote.api

import com.jms.weather.feature.current.data.remote.response.CurrentWeatherGroupResponse
import com.jms.weather.feature.current.data.remote.response.CurrentWeatherResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherMapService {

    @GET("/data/2.5/group")
    suspend fun getCurrentWeatherForGroup(@Query("id") ids:String,
                                          @Query("units") units:String = UNITS_METRIC
                                          ): Response<CurrentWeatherGroupResponse>

}