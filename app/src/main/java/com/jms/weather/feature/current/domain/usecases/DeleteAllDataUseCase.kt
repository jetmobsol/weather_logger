package com.jms.weather.feature.current.domain.usecases

import com.jms.weather.feature.current.domain.repository.WeatherRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@Singleton
class DeleteAllDataUseCase @Inject constructor(
    private val weatherRepository: WeatherRepository
) {

    fun deleteAllData() = weatherRepository.deleteAllData()

}
