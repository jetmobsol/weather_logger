package com.jms.weather.feature.current

import com.jms.weather.feature.current.data.local.WeatherDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class WeatherDaoModule {

    @Singleton
    @Provides
    fun provideCurrentWeatherDao(database: WeatherDatabase) = database.getCurrentWeatherDao()

    @Singleton
    @Provides
    fun provideLocationDao(database: WeatherDatabase) = database.getLocationDao()

}