package com.jms.weather.feature.current.domain.model

import java.util.*

data class CurrentWeatherDomainModel(
    val id: Long,
    val remoteId: Long?=0,
    val name: String? = null,
    val temperature: String?=null,
    val date: String? = null,
    val dateAsMilis: Long = 0L,
    val icon: String? = null,
    val recordCount:Int = 0
)
