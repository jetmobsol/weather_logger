package com.jms.weather.feature.current.data

import com.jms.weather.feature.current.data.local.DbCurrentWeather
import com.jms.weather.feature.current.data.local.DbLocation
import com.jms.weather.feature.current.data.remote.response.CurrentWeatherGroupResponse
import com.jms.weather.feature.current.data.remote.response.CurrentWeatherResponse
import com.jms.weather.feature.current.domain.model.CurrentWeatherDomainModel
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter

var CUSTOM_FORMATER = DateTimeFormatter.ofPattern("HH:mm:ss dd.MM.yyyy")

fun CurrentWeatherGroupResponse.toDbLocationList(): List<DbLocation> {
    return this.list?.map { weatherResponse ->
        val location = DbLocation(
            id = 0L,
            remoteId = weatherResponse.id?.toLong() ?: 0L,
            locationName = weatherResponse.name,
            lat = weatherResponse.coord?.lat,
            lon = weatherResponse.coord?.lon,
            country = weatherResponse.sys?.country
        )
        location.weatherList.add(weatherResponse.toDbCurrentWeather())
        location

    }?.toList() ?: listOf()
}

fun List<DbLocation>.toLocationDomainModelList() =
    this.map { it.toLocationDomainModel() }


fun DbLocation.toLocationDomainModel(): CurrentWeatherDomainModel {
    val weather = this.weatherList.firstOrNull()
    return CurrentWeatherDomainModel(
        id = this.id,
        remoteId = this.remoteId,
        name = this.locationName,
        temperature = if (weather?.temperature != null) "%.2f °C".format(weather.temperature) else "Unknown",
        date = if (weather?.requestTime != null) CUSTOM_FORMATER.format(millsToLocalDateTime(weather.requestTime)) else null,
        dateAsMilis = weather?.requestTime ?: 0L,
        icon = weather?.icon,
        recordCount = this.recordCount
    )
}


fun CurrentWeatherResponse.toDbCurrentWeather(): DbCurrentWeather {
    return DbCurrentWeather(
        id = 0L,
        serverTime = this.dt?.toLong() ?: 0L,
        temperature = this.main?.temp,
        icon = this.weather?.first()?.icon
    )
}


fun millsToLocalDateTime(millis: Long): LocalDateTime? {
    val instant = Instant.ofEpochMilli(millis)
    return instant.atZone(ZoneId.systemDefault()).toLocalDateTime()
}
