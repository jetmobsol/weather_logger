package com.jms.weather.feature.current.data.local.dao

import androidx.room.*
import com.jms.weather.feature.current.data.local.DbCurrentWeather
import com.jms.weather.feature.current.data.local.DbLocation
import kotlinx.coroutines.flow.Flow

/**
 * Data Access Object (DAO) for [com.jms.weather.feature.current.data.local.WeatherDatabase]
 */
@Dao
abstract class LocationDao {

    /**
     * Inserts [DbLocation] into the [DbLocation.TABLE_NAME] table.
     * Duplicate values are replaced in the table.
     * @param locations DbLocation
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertLocation(location: DbLocation):Long


    @Query("SELECT * from ${DbLocation.TABLE_NAME} WHERE remoteId= :remoteId")
    abstract fun getItemByRemoteId(remoteId: Long): DbLocation?


    @Query("SELECT * FROM ${DbLocation.TABLE_NAME} order by id desc")
    abstract fun getAllLocations(): Flow<List<DbLocation>>


    /**
     * Deletes all the items from the [DbCurrentWeather.TABLE_NAME] table.
     */
    @Query("DELETE FROM ${DbLocation.TABLE_NAME}")
    abstract fun deleteAllLocations()


}